#ifndef USERMANAGEMENT_H
#define USERMANAGEMENT_H

int userPush(user_t * userHead, char userFirstName[30], char userLastName[30],
    char userEmailAddress[50], char userPassword[30], bool userAdmin);

int userPop(user_t ** userHead);

int userRemoveByIndex(user_t ** userHead, int index);

int userPrintByIndex(user_t ** userHead, int index);

void userPrint(user_t * userHead);

int userPrintAllBooks(user_t * user);

int userListAll(user_t * userHead);

int userWriteToFile(user_t * userHead);

int userReadFile(user_t * userHead);

#endif /*BOOKMANAGMENT */
