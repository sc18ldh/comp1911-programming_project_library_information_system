# COMP1911-Programming_Project_Library_Information_System

Programming project written in C for a Library Information System.

The project utilises a spreadsheet and terminal interface to allow a user to create an account as well as loan and return books.

Utilises CMake for project compilation. 