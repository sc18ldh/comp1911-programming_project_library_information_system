#include <stdio.h>
#include <stdlib.h>
#include "math.h"
#include "stdbool.h"
#include <string.h>

#include "bookStructure.h"
#include "userStructure.h"
#include "userManagement.h"


bool isFirstBook = true;
/*
function pushes new item on to the top of the stack

@Param book struct, string of title, string of date in dd/mm/yy format
and boolean or integer (0 or 1) value for available

@Return returns 0 if book can be added 1 if not
*/
int bookPush(book_t * bookHead, char userTitle[50], char userLoanDate[9],
    bool useravailable, int userPages, char userloanedTo[50], int userLoanPeriod) {
    book_t * current = bookHead;


    if(isFirstBook == true) {
          strcpy(current->title, userTitle);
          strcpy(current->loanDate, userLoanDate);
          current->available = useravailable;

          current->pages = userPages;
          strcpy(current->loanedTo, userloanedTo);
          current->loanPeriod = userLoanPeriod;

          isFirstBook = false;
          return 0;
    }


    //itterating over list until we reach the end
    while (current->next != NULL) {
        //preventing multiple instances of the same book
        if(strcmp(userTitle, current->title) == 0) {
          return 1;
        }

        current = current->next;
    }

    //if the current book is identical then don't add
    if(strcmp(userTitle, current->title) == 0) {
      return 1;
    }

  //adding new book to list
    current->next = malloc(sizeof(book_t));
    strcpy(current->next->title, userTitle);
    strcpy(current->next->loanDate, userLoanDate);
    current->next->available = useravailable;

    current->next->pages = userPages;
    strcpy(current->next->loanedTo, userloanedTo);;
    current->next->loanPeriod = userLoanPeriod;

    current->next->next= NULL;
    return 0;
}

/*
removes first item from the stack

@Param address of book struct

@Return 0 if can remove 1 if pointer is null
*/
int bookPop(book_t ** bookHead) {
    book_t * next_book = NULL;

    //if the head of the list does not exits then end
    if (*bookHead == NULL) {
        return 1;
    }

    next_book = (*bookHead)->next;
    free(*bookHead);
    *bookHead = next_book;
    return 0;
}

/*
Removes item from stack at a given index

@Param address of book and index of item that will be removed

@Return 0 if can remove 1 if pointer at index is null
*/
int bookRemoveByIndex(book_t ** bookHead, int index) {
    int i = 0;
    book_t * current = *bookHead;
    book_t * temp_book = NULL;

    //if the index is zero pop first item in the list
    if (index == 0) {
        return bookPop(bookHead);
    }

    //removing indexed item
    for (i = 0; i < index -1; i++) {
        if (current->next == NULL) {
            return 1;
        }
        current = current->next;
    }

    temp_book = current->next;
    current->next = temp_book->next;
    free(temp_book);

    return 0;
}

/*
Prints out book at given index

@Param adress of book structure and idex of printed book

@Return 1 if null and 0 if item can be printed
*/
int bookPrintByIndex(book_t ** bookHead, int index) {

  int i = 0;
  book_t * current = *bookHead;
  book_t * temp_book = NULL;

  //if the index is zero we print the first element in the list
  if (index == 0) {
      printf("\n\nTitle: %s", current->title);
      //testing if book is available:
      if(current->available == false){
        printf("\nAvailable: NO");
      }

      else {
        printf("\nAvailable: YES");
      }
      printf("\nLast Loan Date: %s", current->loanDate);
      printf("\nNumber of pages: %d", current->pages);
      printf("\nloaned to: %s", current->loanedTo);
      printf("\nLoan period (weeks): %d", current->loanPeriod);


      return 0;

  }

  for (i = 0; i < index; i++) {
      if (current->next == NULL) {
          return 1;
      }
      current = current->next;
  }

  //printing out book at given index
  printf("\n\nTitle: %s", current->title);
  //testing if book is available:
  if(current->available == false){
    printf("\nAvailable: NO");
  }

  else {
    printf("\nAvailable: YES");
  }
  printf("\nLast Loan Date: %s", current->loanDate);
  printf("\nNumber of pages: %d", current->pages);
  printf("\nloaned to: %s", current->loanedTo);
  printf("\nLoan period (weeks): %d", current->loanPeriod);

  return 0;
}

/*
Prints given book

@Param book struct

@Return 0 if can be printed 1 if it cannot
*/
void bookPrint(book_t * bookHead) {

  book_t * current = bookHead;

  printf("\n\nTitle: %s", current->title);
  //testing if book is available:
  if(current->available == false){
    printf("\nAvailable: NO");
  }

  else {
    printf("\nAvailable: YES");
  }
  printf("\nLast Loan Date: %s", current->loanDate);
  printf("\nNumber of pages: %d", current->pages);
  printf("\nloaned to: %s", current->loanedTo);
  printf("\nLoan period (weeks): %d", current->loanPeriod);
}

/*
Returns number of elements in the stack

@Param book structure

@Return integer number of books
*/
int booksNumOf(book_t * bookHead) {
    int numberOfBooks = 0;
    book_t * current = bookHead;

    //itterating over list and counting elements
    while (current != NULL) {
        ++numberOfBooks;
        current = current->next;
    }
  return numberOfBooks;
}

/*
Prints out all books in the list

@Param head of list

@Return 1 if the head is null (no books) and 0 if it can be completed
*/
int bookListAll(book_t * bookHead) {
    int index = 0;
    book_t * current = bookHead;

    //ensuring we have at least one book to display
    if(current == NULL) {
      return 1;
    }

    //printing out all books in list
    while (current != NULL) {
        printf("\n\n###################################");
        printf("\nBook ID(%d)", index + 1);
        printf("\n###################################");

        bookPrintByIndex(&bookHead, index);
        current = current->next;
        ++index;
    }
    return 0;
}

int bookListWithString(book_t * bookHead, char userSearch[50]) {
    int index = 0;
    book_t * current = bookHead;

    //ensuring we have at least one book to display
    if(current == NULL) {
      return 1;
    }

    //printing out all books in list
    while (current != NULL) {
        if(strstr(current->title, userSearch ) != NULL ){
        printf("\n\n###################################");
        printf("\nBook ID(%d)", index + 1);
        printf("\n###################################");

        bookPrintByIndex(&bookHead, index);
      }
        current = current->next;
        ++index;
    }
    return 0;
}

/*
Writes entire list of books to csv file named bookDatabase.csv

@Param head of book list

@Return 0 if can be written in full and 1 if there are errors
*/
int bookWriteToFile(book_t * bookHead) {
  FILE * fPointer;
  fPointer = fopen("bookDatabase.csv", "w");

  int index = 0;
  book_t * current = bookHead;

  //ensuring we have at least one book to display
  if(current == NULL) {
    return 1;
  }

  //writing to file
  while (current != NULL) {
      fflush(stdout);
      fprintf(fPointer, "%s,%d,%s,%d,%s,%d\n", current->title, current->available,
      current->loanDate, current->pages, current->loanedTo, current->loanPeriod);

      current = current->next;
      ++index;
  }

  fclose(fPointer);

  return 0;
}

book_t * bookReturnByIndex(book_t ** bookHead, int index) {

  int i = 0;
  book_t * current = *bookHead;


  //if the index is zero we print the first element in the list
  if (index == 0) {
        return current;
  }

  for (i = 0; i < index; i++) {
      current = current->next;
  }

  return current;

}
/*
Returns the book given a title string

@Param head of book list and the desired title to find

@return Returns book with title and the boohHEad otherwise

*/
book_t * bookReturnByTitle(book_t * bookHead, char userTitle[50]) {
  book_t * current = bookHead;

  while (current->next != NULL) {
    if(strcmp(current->title, userTitle) == 0) {
      break;
    }
    current = current->next;
  }

  return current;
}


/*
Reads all of information from csv and adds book to list

@Param head of book list

@Return 0 if all books can be added to list and 1 otherwise
*/
int bookReadFile(book_t * bookHead, user_t * userHead) {
  book_t * current = bookHead;
  char line[150];
  int lineIndex = 0;
  int tempStringIndex = 0;
  char currentChar;

  char titleString[50];
  bool isAvailable;
  char loanDataString[9];
  char pageString[5];
  int pageNum;
  char loanedToString[50];
  bool isFirstBook = true;
  int loanPeriodInt;

  //opening and reading file
  FILE * fPointer;
  fPointer = fopen("bookDatabase.csv", "r+");

  while(fgets(line, 60, fPointer)!= NULL){
    user_t * userCurrent = userHead;
    if(line[0] == '\0'){
      printf("nothing in file");
    }
    lineIndex = 0;
    tempStringIndex = 0;
    currentChar = ' ';

    //resting title string
    memset(&titleString[0], 0, sizeof(titleString));
    memset(&loanDataString[0], 0, sizeof(loanDataString));
    memset(&pageString[0], 0, sizeof(pageString));
    memset(&loanedToString[0], 0, sizeof(loanedToString));

    //adding book to database
    // adding title
    while(currentChar != ','){
      currentChar = line[lineIndex];
      if(currentChar == ','){
        break;
      }

      titleString[tempStringIndex] = currentChar;
      ++lineIndex;
      ++tempStringIndex;
    }

    //if available
    lineIndex += 1;
    tempStringIndex = 0;
    currentChar = ' ';

    //parsing availaiblity name
    while(currentChar != ','){
      currentChar = line[lineIndex];
      if(currentChar == ','){
        break;
      }

      ++lineIndex;
      ++tempStringIndex;
    }

    currentChar = line[lineIndex - 1];
    isAvailable = (int) (currentChar - '0');

    //parsing loan date
    lineIndex += 1;
    tempStringIndex = 0;
    currentChar = ' ';

    while(currentChar != ','){
      currentChar = line[lineIndex];
      if(currentChar == ','){
        break;
      }

      loanDataString[tempStringIndex] = currentChar;
      ++lineIndex;
      ++tempStringIndex;
    }

    lineIndex += 1;
    tempStringIndex = 0;
    currentChar = ' ';

    //parsing pages
    while(currentChar != ','){
      currentChar = line[lineIndex];
      if(currentChar == ','){
        break;
      }

      pageString[tempStringIndex] = currentChar;
      ++lineIndex;
      ++tempStringIndex;
    }

    pageNum = atoi(pageString);

    lineIndex += 1;
    tempStringIndex = 0;
    currentChar = ' ';

    //parsing loanedTo
    while(currentChar != ','){
      currentChar = line[lineIndex];
      if(currentChar == ','){
        break;
      }

      loanedToString[tempStringIndex] = currentChar;
      ++lineIndex;
      ++tempStringIndex;
    }

    ///parsing loan period
    lineIndex += 1;
    tempStringIndex = 0;

    currentChar = line[lineIndex];
    loanPeriodInt = (currentChar - '0');


    //adding file tprintf("\nLoan period (weeks): %d", current->loanPeriod);o book list

    bookPush(bookHead, titleString, loanDataString, isAvailable, pageNum,
      loanedToString, loanPeriodInt);

    //adding books to user

    //testing if there is a user who owns the book


    if(booksNumOf(bookHead) > 1) {
    current = current->next;
    }

    if(strcmp(loanedToString, "NONE") != 0) {
      while (userCurrent != NULL) {
        if(strcmp(loanedToString, userCurrent->emailAddress) == 0) {
          userCurrent->book[userCurrent->numLoanedBooks] = current;
          ++userCurrent->numLoanedBooks;
          // printf("\n\nBOOK TITLE %s", userCurrent->book[0]->title);
        }
        userCurrent = userCurrent->next;
      }
    }
  }
  fclose(fPointer);
}
