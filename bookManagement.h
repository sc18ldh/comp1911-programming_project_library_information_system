#ifndef BOOKMANAGEMENT_H
#define BOOKMANAGEMENT_H

#include "userStructure.h"
int bookPush(book_t * bookHead, char userTitle[50], char userLoanDate[9],
    bool useravailable, int userPages, char userloanedTo[50], int userLoanPeriod);

int bookPop(book_t ** bookHead);

int bookRemoveByIndex(book_t ** bookHead, int index);

int bookPrintByIndex(book_t ** bookHead, int index);

void bookPrint(book_t * bookHead);

int booksNumOf(book_t * bookHead);

int bookListAll(book_t * bookHead);

int bookListWithString(book_t * bookHead, char userSearch[50]);

book_t * bookReturnByIndex(book_t ** bookHead, int index);

book_t * bookReturnByTitle(book_t * bookHead, char userTitle[50]);

int bookWriteToFile(book_t * bookHead);

int bookReadFile(book_t * bookHead, user_t * userHead);

#endif /*BOOKMANAGMENT */
