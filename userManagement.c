#include <stdio.h>
#include <stdlib.h>
#include "math.h"
#include "stdbool.h"
#include <string.h>

#include "bookStructure.h"
#include "bookManagement.h"
#include "userStructure.h"
// #include "userStructure.h"

bool isFirstuser = true;



/*
function pushes new item on to the top of the stack

@Param user struct, string of title, string of date in dd/mm/yy format
and boolean or integer (0 or 1) value for available

@Return returns 0 if user can be added 1 if not
*/
int userPush(user_t * userHead, char userFirstName[30], char userLastName[30],
    char userEmailAddress[50], char userPassword[30], bool userAdmin) {

    user_t * current = userHead;

    if(isFirstuser == true) {
          strcpy(current->firstName, userFirstName);
          strcpy(current->lastName, userLastName);
          strcpy(current->emailAddress, userEmailAddress);
          strcpy(current->password, userPassword);
          current->numLoanedBooks = 0;
          current->admin = userAdmin;
          isFirstuser = false;
          return 0;
    }

    //itterating over list until we reach the end
    while (current->next != NULL) {
        //preventing multiple instances of the same user
        if(strcmp(userEmailAddress, current->emailAddress) == 0) {
          return 1;
        }

        current = current->next;
    }

    //if the current user is identical then don't add
    if(strcmp(userEmailAddress, current->emailAddress) == 0) {
      return 1;
    }

  //adding new user to list
    current->next = malloc(sizeof(user_t));
    strcpy(current->next->firstName, userFirstName);
    strcpy(current->next->lastName, userLastName);
    strcpy(current->next->emailAddress, userEmailAddress);
    strcpy(current->next->password, userPassword);
    current->next->admin = userAdmin;
    current->next->numLoanedBooks = 0;

    current->next->next = NULL;

    for(int i = 0; i < 4; ++i){
      current->next->book[i] = NULL;
    }
    return 0;

    for(int i = 0; i < 4; ++i){
      current->book[i] = NULL;
    }
    return 0;
}

/*
removes first item from the stack

@Param address of user struct

@Return 0 if can remove 1 if pointer is null
*/
int userPop(user_t ** userHead) {
    user_t * next_user = NULL;

    //if the head of the list does not exits then end
    if (*userHead == NULL) {
        return 1;
    }

    next_user = (*userHead)->next;
    free(*userHead);
    *userHead = next_user;
    return 0;
}

/*
Removes item from stack at a given index

@Param address of user and index of item that will be removed

@Return 0 if can remove 1 if pointer at index is null
*/
int userRemoveByIndex(user_t ** userHead, int index) {
    int i = 0;
    user_t * current = *userHead;
    user_t * temp_user = NULL;

    //if the index is zero pop first item in the list
    if (index == 0) {
        return userPop(userHead);
    }

    //removing indexed item
    for (i = 0; i < index -1; i++) {
        if (current->next == NULL) {
            return 1;
        }
        current = current->next;
    }

    temp_user = current->next;
    current->next = temp_user->next;
    free(temp_user);

    return 0;
}

/*
Prints out user at given index

@Param adress of user structure and idex of printed user

@Return 1 if null and 0 if item can be printed
*/
int userPrintByIndex(user_t ** userHead, int index) {

  int i = 0;
  user_t * current = *userHead;
  user_t * temp_user = NULL;

  //if the index is zero we print the first element in the list
  if (index == 0) {
      printf("\n\nFirst Name    : %s", current->firstName);
      printf("\nLast Name     : %s", current->lastName);
      printf("\nEmail Address : %s", current->emailAddress);
      printf("\nBooks Loaned  : %d", current->numLoanedBooks);

      //testing if user is available:
      if(current->admin == false){
        printf("\nAdmin: NO");
      }

      else {
        printf("\nAdmin: YES");
      }
      return 0;
  }

  for (i = 0; i < index; i++) {
      if (current->next == NULL) {
          return 1;
      }
      current = current->next;
  }

  //printing out user at given index
  printf("\n\nFirst Name    : %s", current->firstName);
  printf("\nLast Name     : %s", current->lastName);
  printf("\nEmail Address : %s", current->emailAddress);
  printf("\nBooks Loaned  : %d", current->numLoanedBooks);
  //testing if user is available:
  if(current->admin == false){
    printf("\nAdmin: NO");
  }

  else {
    printf("\nAdmin: YES");
  }
  return 0;
}

/*
Prints given user

@Param user struct

@Return 0 if can be printed 1 if it cannot
*/
void userPrint(user_t * userHead) {
  //printing out user at given index
  printf("\n\nFirst Name    : %s", userHead->firstName);
  printf("\nLast Name     : %s", userHead->lastName);
  printf("\nEmail Address : %s", userHead->emailAddress);
  printf("\nBooks Loaned  : %d", userHead->numLoanedBooks);
  //testing if user is available:
  if(userHead->admin == false){
    printf("\nAdmin: NO");
  }

  else {
    printf("\nAdmin: YES");
  }
}

int userPrintAllBooks(user_t * user) {
  int numBooksPrinted = 0;
  user_t * current = user;

  for(int i = 0; i < 4; ++i) {
    if(user->book[i] != NULL) {
      printf("\n\n###################################");
      printf("\nBOOK NUMBER (%d)", i + 1);
      printf("\n###################################");
      bookPrint(user->book[i]);

      ++numBooksPrinted;
    }
  }

  if(numBooksPrinted == 0) {
    printf("\n\nNo books to Return!");
    return 1;
  }

  return 0;
}

/*
Returns number of elements in the stack

@Param user structure

@Return integer number of users
*/
int usersNumOf(user_t * userHead) {
    int numberOfusers = 0;
    user_t * current = userHead;

    //itterating over list and counting elements
    while (current != NULL) {
        ++numberOfusers;
        current = current->next;
    }
  return numberOfusers;
}


/*
Prints out all users in the list

@Param head of list

@Return 1 if the head is null (no users) and 0 if it can be completed
*/
int userListAll(user_t * userHead) {
    int index = 0;
    user_t * current = userHead;

    //ensuring we have at least one user to display
    if(current == NULL) {
      return 1;
    }

    //printing out all users in list
    while (current != NULL) {
        printf("\n\n###################################");
        printf("\nUser Number(%d)", index + 1);
        printf("\n###################################");

        userPrintByIndex(&userHead, index);
        current = current->next;
        ++index;
    }
    return 0;
}

/*
Writes entire list of users to csv file named userDatabase.csv

@Param head of user list

@Return 0 if can be written in full and 1 if there are errors
*/
int userWriteToFile(user_t * userHead) {
  FILE * fPointer;
  fPointer = fopen("userDatabase.csv", "w");

  int index = 0;
  user_t * current = userHead;

  //ensuring we have at least one user to display
  if(current == NULL) {
    return 1;
  }

  //writing to file
  while (current != NULL) {
      fprintf(fPointer, "%s,%s,%s,%s,%d\n", current->firstName,
      current->lastName, current->emailAddress, current->password, current->admin);

      current = current->next;
      ++index;
  }

  fclose(fPointer);

  return 0;
}

int userReadFile(user_t * userHead) {
  user_t * current = userHead;
  char line[150];
  int lineIndex = 0;
  int tempStringIndex = 0;
  char currentChar;

  char firstNameString[30];
  char lastNameString[30];
  char emailString[30];
  char passwordString[30];
  bool isAdmin;
  char isAdminString[10];

  bool isFirstuser = true;

  //opening and reading file
  FILE * fPointer;
  fPointer = fopen("userDatabase.csv", "r+");

  while(fgets(line, 100, fPointer)!= NULL){
    if(line[0] == '\0'){
      printf("\n\nnothing in file");
    }
    lineIndex = 0;
    tempStringIndex = 0;
    currentChar = ' ';

    //resting all strings
    memset(&firstNameString[0], 0, sizeof(firstNameString));
    memset(&lastNameString[0], 0, sizeof(lastNameString));
    memset(&emailString[0], 0, sizeof(emailString));
    memset(&passwordString[0], 0, sizeof(passwordString));

    //adding user to database

    //parsing first name
    while(currentChar != ','){
      currentChar = line[lineIndex];
      if(currentChar == ','){
        break;
      }

      firstNameString[tempStringIndex] = currentChar;
      ++lineIndex;
      ++tempStringIndex;
    }

    lineIndex += 1;
    tempStringIndex = 0;
    currentChar = ' ';

    //parsing last name
    while(currentChar != ','){
      currentChar = line[lineIndex];
      if(currentChar == ','){
        break;
      }

      lastNameString[tempStringIndex] = currentChar;
      ++lineIndex;
      ++tempStringIndex;
    }

    lineIndex += 1;
    tempStringIndex = 0;
    currentChar = ' ';

    //parsing email address
    while(currentChar != ','){
      currentChar = line[lineIndex];
      if(currentChar == ','){
        break;
      }

      emailString[tempStringIndex] = currentChar;
      ++lineIndex;
      ++tempStringIndex;
    }

    lineIndex += 1;
    tempStringIndex = 0;
    currentChar = ' ';

    while(currentChar != ','){
      currentChar = line[lineIndex];
      if(currentChar == ','){
        break;
      }

      passwordString[tempStringIndex] = currentChar;
      ++lineIndex;
      ++tempStringIndex;
    }

    //parsing is admin

    lineIndex += 1;
    isAdminString[0] = line[lineIndex];

    isAdmin = atoi(isAdminString);

    //adding file to user list

    userPush(userHead, firstNameString, lastNameString, emailString,
      passwordString, isAdmin);

  }
  fclose(fPointer);
}

int userAddBook(user_t * userHead, book_t * bookHead) {
    book_t * current = bookHead;


  return 0;
}

// int main() {
//   // printf("\nNum of users: %d\n",usersNumOf(userHead));
//   user_t *userHead = NULL; //inital value of null
//   userHead = malloc(sizeof(user_t));
//   if (userHead == NULL) {
//       return 1;
//   }
//
//   book_t *bookHead = NULL; //inital value of null
//   bookHead = malloc(sizeof(book_t));
//   if (bookHead == NULL) {
//       return 1;
//   }
//
//
//
//   // userPush(userHead, "Luke", "harrison", "emai2l@email.com", "edgw", true);
//   // // userPrint(userHead);
//   //
//   // userPush(userHead, "Joe", "Bloggs", "email@email.com", "dodgey", false);
//   // // userPrint(userHead);
//   // userPush(userHead, "Jamie", "Bloggs", "emai3l@email.com", "dodgey", true);
//   // printf("Num of users: %d", usersNumOf(userHead));
//   // userPrintByIndex(&userHead, 0);
//   // userPrintByIndex(&userHead, 1);
//   // userRemoveByIndex(&userHead, 0);
//   // userPrint(userHead);
//
//   // userWriteToFile(userHead);
//   userReadFile(userHead);
//   userWriteToFile(userHead);
//   userListAll(userHead);
//   userWriteToFile(userHead);
//
//
//
//
//
//   return 0;
// }
