#ifndef BOOKSTRUCTURE_H
#define BOOKSTRUCTURE_H

/*
A structure for creating and adding books
*/
typedef struct book {
  char title[50];
  bool available;
  char loanDate[6];

  int pages;
  char author[50];
  char loanedTo[50];
  int loanPeriod;
  struct book * next;
} book_t;

#endif /*BOOKSTRUCTURE */
