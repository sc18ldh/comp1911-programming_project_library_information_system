
/* Author: Matteo Leonetti m.leonetti@leeds.ac.uk
 *
 * Contanct the instructors if this code does not compile, or for any other related problem.
 *
 *
 */
 #include <stdio.h>
 #include <stdlib.h>
 #include "math.h"
 #include "stdbool.h"
 #include <string.h>

 #include "bookStructure.h"
 #include "bookManagement.h"
 #include "userStructure.h"
 #include "userManagement.h"

#include "unity.h"





void testBookManagement (void ) {

	book_t *bookHead = NULL; //inital value of null

	//checking that head cannot be popped when NULL
	TEST_ASSERT_EQUAL_INT(1, bookPop(&bookHead));

	bookHead = malloc(sizeof(book_t));

	//adding book
	TEST_ASSERT_EQUAL_INT(0, bookPush(bookHead, "book1", "06/11/99", 1, 500, "NONE", 4 ));

	//checking that correct number of books has been detected
	TEST_ASSERT_EQUAL_INT(1, booksNumOf(bookHead));

	//checking that title is correct
	TEST_ASSERT_EQUAL_INT(0, strcmp(bookHead->title, "book1"));

	//checking that the same book cannot be added twice
	TEST_ASSERT_EQUAL_INT(1, bookPush(bookHead, "book1", "06/11/99", 1, 500, "NONE", 4 ));

	//checking that correct number of books has been detected
	TEST_ASSERT_EQUAL_INT(1, booksNumOf(bookHead));

	//adding another new book
	TEST_ASSERT_EQUAL_INT(0, bookPush(bookHead, "book2", "06/11/99", 1, 500, "NONE", 4 ));

	//checking that correct number of books has been detected
	TEST_ASSERT_EQUAL_INT(2, booksNumOf(bookHead));

	//adding another new book
	TEST_ASSERT_EQUAL_INT(0, bookPush(bookHead, "book3", "06/11/99", 1, 500, "NONE", 4 ));

	//checking that correct number of books has been detected
	TEST_ASSERT_EQUAL_INT(3, booksNumOf(bookHead));

	bookRemoveByIndex(&bookHead, 2);

	//checking that correct number of books has been detected
	TEST_ASSERT_EQUAL_INT(2, booksNumOf(bookHead));

	book_t * current = bookReturnByIndex(&bookHead, 1);

	//checking that correct book has been returned
	TEST_ASSERT_EQUAL_INT(0, strcmp("book2", current->title));

}

void testUserManagement (void) {

	//initialising linked lists
	user_t *userHead = NULL; //inital value of null

	//checking that head cannot be popped when NULL
	TEST_ASSERT_EQUAL_INT(1, userPop(&userHead));

	userHead = malloc(sizeof(user_t));

	//adding user
	TEST_ASSERT_EQUAL_INT(0, userPush(userHead, "Luke", "Harrison", "email@email.com", "password", 0));

	//checking that same user cannot be added twice
	TEST_ASSERT_EQUAL_INT(1, userPush(userHead, "Luke", "Harrison", "email@email.com", "password", 0));

	//adding amother user
	TEST_ASSERT_EQUAL_INT(0, userPush(userHead, "Tom", "Harrison", "Tom@email.com", "password", 0));

	//checking that everything has added correctly

	TEST_ASSERT_EQUAL_INT(0, strcmp(userHead->next->firstName, "Tom"));

}



void setUp ( void ) {}

void tearDown ( void ) {}

int main ( void ) {
	UNITY_BEGIN();


	RUN_TEST ( testBookManagement );
	RUN_TEST ( testUserManagement );

	return UNITY_END();
}
