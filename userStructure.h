#ifndef USERSTRUCTURE_H
#define USERSTRUCTURE_H

typedef struct user {
    char firstName[30];
    char lastName[30];
    char emailAddress[50];
    char password[30];
    bool admin;
    //
    int numLoanedBooks;
    book_t * book[4];
    struct user * next;
} user_t;

#endif /*BOOKMANAGMENT */
