#include <stdio.h>
#include <stdlib.h>
#include "math.h"
#include "stdbool.h"
#include <string.h>

#include "bookStructure.h"
#include "bookManagement.h"
#include "userStructure.h"
#include "userManagement.h"


int main() {

  //initialising linked lists
  user_t *userHead = NULL; //inital value of null
  userHead = malloc(sizeof(user_t));
  if (userHead == NULL) {
      return 1;
  }

  book_t *bookHead = NULL; //inital value of null
  bookHead = malloc(sizeof(book_t));
  if (bookHead == NULL) {
      return 1;
  }

  //local variables
  user_t * userCurrent = userHead;
  user_t * userLoggedIn = userHead;
  book_t * bookCurrent = bookHead;

  char adminPassword[] = "password123";
  char userInputString[80];

  //reading from files to get previous stored information
    userReadFile(userHead);
    bookReadFile(bookHead, userHead);

  //############################## MAIN MENU ###################################
  printf("\n\n##################################################################");
  printf("\n------------------------- LIBRARY SYSTEM ------------------------- ");

  while(1) {
    printf("\n##################################################################");
    printf("\nPAGE: MAIN MENU");

    printf("\n\nWelcome to the library system!");
    printf("\n\n(1) to register as a new user type '1'");
    printf("\n(2) to login type '2'");
    printf("\n(3) to exit the program type 'exit' or '3'");

    printf("\n\nPlease enter input: ");
    scanf(" %s", userInputString);

    //############################## REGISTER USER #############################
    if(strcmp(userInputString, "1") == 0) {
      printf("\n##################################################################");
      printf("\nPAGE: REGISTER USER");
      printf("\nType 'exit' to return to the main menu");

      //declaring variables
      char userEmailAddress[50];
      char userFirstName[30];
      char userLastName[30];
      char userPassword[30];

      //checking that email address is valid
      while(1) {
        userCurrent = userHead;

        memset(&userEmailAddress[0], 0, sizeof(userEmailAddress));
        printf("\n\nPlease enter email address below, email address must not");
        printf("\nalready be registered");

        printf("\n\nEmail: ");
        scanf(" %s", userEmailAddress);

        //exiting page
        if(strcmp(userEmailAddress, "exit") == 0) {
          printf("\nRETURNING!");
          break;
        }

        //checking that email is not already in the system
        while (userCurrent->next != NULL) {
          if(strcmp(userEmailAddress, userCurrent->emailAddress) == 0) {
          printf("\n\n%s and %s are identical", userEmailAddress, userCurrent->emailAddress);
          break;
          }
          userCurrent = userCurrent->next;
        }

        if(strcmp(userEmailAddress, userCurrent->emailAddress) != 0) {
          printf("\n\nEmail address is available!");
          break;
        }

        else {
          printf("\n##################################################################");
          printf("\nERROR: Email address is taken, please try logging in instead");
          printf("\n##################################################################");
        }
      }

      if(strcmp(userEmailAddress, "exit") != 0) {
        printf("\n\nFirst name: ");
        scanf(" %s", userFirstName);
        printf("\nLast name: ");
        scanf(" %s", userLastName);
        printf("\nPassword: ");
        scanf(" %s", userPassword);

        printf("\n\nAre you an Admin (y/n): ");
        scanf(" %s", userInputString);

        //if the user wants admin privalages
        if(strcmp(userInputString, "y") == 0) {
          printf("\n\nTo gain admin privalages on this account you must enter \nthe admin password below");

          printf("\n\nAdmin password: ");
          scanf(" %s", userInputString);

          if(strcmp(userInputString, adminPassword) == 0){
            printf("\n\nPASSWORD ACCEPTED!\nAdmin privalages added\n\nAdding user!");
            userPush(userHead, userFirstName, userLastName, userEmailAddress, userPassword, true);

          }

          else{
            printf("\n\nINCORRECT PASSWWORD!\nNo admin privilages\n\nAdding user!");
            userPush(userHead, userFirstName, userLastName, userEmailAddress, userPassword, false);
          }
        }

        //if the user does NOT want admin privilages
        else{
          printf("\n\nAdding user!");
          userPush(userHead, userFirstName, userLastName, userEmailAddress, userPassword, false);
        }

        printf("\n##################################################################");
        printf("\nPlease login using your new credentials\nTYPE 'n' TO CONTINUE\n");
        scanf(" %s", userInputString);
      }
    }

    //############################## LOGIN USER #############################
    if(strcmp(userInputString, "2") == 0) {
    printf("\n##################################################################");
    printf("\nPAGE: LOGIN");

    while(1) {
      userCurrent = userHead;


      printf("\n\nPlease enter your email address and password below");
      printf("\nType 'exit' to return to the main menu");

      printf("\n\nEmail: ");
      scanf(" %s", userInputString);

      //if the user is already in the system
      while (userCurrent->next != NULL) {
        if(strcmp(userInputString, userCurrent->emailAddress) == 0) {
        break;
        }
        userCurrent = userCurrent->next;
      }
      if(strcmp(userInputString, "exit") == 0) {
        printf("\nRETURNING!");
        break;
      }

      //User has been found
      if(strcmp(userInputString, userCurrent->emailAddress) == 0) {
        printf("\nPlease enter password: ");
        scanf(" %s", userInputString);

        //cehcking password is correct
        if(strcmp(userInputString, userCurrent->password) == 0) {
          printf("\nPassword Correct!\nLogging into system\n");
          //logging in user
          userLoggedIn = userCurrent;

          //############################## ADMIN MENU #############################
          if(userLoggedIn->admin == true){
            printf("\n##################################################################");
            printf("\nPAGE: ADMIN MENU");

            while(1) {
              printf("\n\nPlease enter your selection from the options below");

              printf("\n\n(1) to list all books type '1'");
              printf("\n(2) to list all users type '2'");
              printf("\n(3) to add a book to the library type '3' ");
              printf("\n(4) to remove a book from the library type '4'");
              printf("\n(5) to logout type '5'");

              printf("\n\nPlease enter input: ");
              scanf(" %s", userInputString);

              //listing all books
              if(strcmp(userInputString, "1") == 0) {
                printf("\n##################################################################");
                printf("\nPAGE: LIST ALL BOOKS");

                printf("\n\nWould you like to list all books (y/n): ");
                scanf(" %s", userInputString);

                if(strcmp(userInputString, "y") == 0){
                  printf("\n\nList of all books:\n ");

                  bookListAll(bookHead);

                  printf("\n\n##################################################################");
                  printf("\nTYPE 'n' to return to Admin menu\n");
                  scanf(" %s", userInputString);
                  printf("\n##################################################################");

                }

                else {
                  printf("\nRETURNING!");
                  printf("\n##################################################################");
                }
              }

            //listing all users
            if(strcmp(userInputString, "2") == 0) {
              printf("\n##################################################################");
              printf("\nPAGE: LIST ALL USERS");

              printf("\n\nWould you like to list all users (y/n): ");
              scanf(" %s", userInputString);

              if(strcmp(userInputString, "y") == 0){
                printf("\n\nList of all users:\n ");

                userListAll(userHead);

                printf("\n\n##################################################################");
                printf("\nTYPE 'n' to return to Admin menu\n");
                scanf(" %s", userInputString);
                printf("\n##################################################################");

              }

              else {
                printf("\nRETURNING!");
                printf("\n##################################################################");
              }
            }

            //adding book
            if(strcmp(userInputString, "3") == 0) {
              char userTitle[50];
              char loanDate[6];
              bool isAvailable = true;
              int userPages;
              char userloanedTo[50];
              int userLoanPeriod;

              printf("\n##################################################################");
              printf("\nPAGE: ADD BOOK");

              while(1) {
                bookCurrent = bookHead;
                printf("\n\nBook title: ");
                scanf(" %s", userTitle);

                //checking that book is not already in the system
                while (bookCurrent->next != NULL) {
                    if(strcmp(userTitle, bookCurrent->title) == 0){
                      break;

                    }
                    bookCurrent = bookCurrent->next;
                }

                //if the book is not in the system already
                if(strcmp(userTitle, bookCurrent->title) != 0) {
                  printf("\nNumber of pages: ");
                  scanf(" %d", &userPages);
                  printf("\nLoan period (weeks): ");
                  scanf(" %d", &userLoanPeriod);

                  printf("\nPlease enter todays date in 'dd/mm/yy' form: ");
                  scanf(" %s", loanDate );

                  printf("\nIs all the information entered correct (y/n): ");
                  scanf("  %s", userInputString);

                  if(strcmp(userInputString, "y") == 0) {
                    printf("\n\nADDING BOOK!");
                    bookPush(bookHead, userTitle, loanDate, true, userPages, "NONE", userLoanPeriod);
                    printf("\n##################################################################");
                    break;
                  }

                  else{
                    printf("\nRETURNING");
                    printf("\n##################################################################");
                    break;
                  }

                }

                //if the book has already been added
                else {
                  printf("\n##################################################################");
                  printf("\nERROR: Book Title has already been added, add another book");
                  printf("\n##################################################################");
                }
              }
            }

            //removing book
            if(strcmp(userInputString, "4") == 0) {
              printf("\n##################################################################");
              printf("\nPAGE: REMOVE BOOK");

              printf("\nTo remove a book please enter a book ID number\nthis can be found by listing all the books");
              printf("\n\nTo exit type 'exit'");

              while(1){
                int indexToRemove;

                printf("\n\nPlease enter book ID to remove: ");
                scanf(" %s", userInputString);

                if(strcmp(userInputString, "exit") == 0) {
                  printf("\n\nRETURNING!");
                  printf("\n##################################################################");
                  break;
                }

                indexToRemove = atoi(userInputString);
                //making sure index is in range
                if(indexToRemove > 0 && indexToRemove <= booksNumOf(bookHead)){


                  printf("\n\n###################################");
                  bookPrintByIndex(&bookHead, (indexToRemove - 1));
                  printf("\n\n###################################");

                  printf("\n\nWould you like to remove the following book(y/n): ");
                  scanf(" %s", userInputString);

                  if(strcmp(userInputString, "y") == 0) {
                    bookRemoveByIndex(&bookHead, indexToRemove - 1);
                    printf("REMOVING BOOK!");
                    printf("\n##################################################################");
                    break;
                  }

               }

               else {
                 printf("\n##################################################################");
                 printf("\nERROR: ID is not in range, must be less than total number of books");
                 printf("\n##################################################################");
               }
             }
            }

            if(strcmp(userInputString, "5") == 0) {
              printf("\n\nRETURNING!");
              printf("\n##################################################################");
              break;
            }
          }
        }

          //############################## USER MENU #############################
          else{
            while(1) {
            printf("\n##################################################################");
            printf("\nPAGE: USER MENU");
            printf("\n\nUSER: %s", userLoggedIn->emailAddress);
            printf("\nNUMBER OF BOOKS LOANED: %d", userLoggedIn->numLoanedBooks);

            printf("\n\nPlease enter your selection from the options below");

            printf("\n\n(1) to list all books type '1'");
            printf("\n(2) to search and borrow a book type '2'");
            printf("\n(3) to return a book type '3'");
            printf("\n(4) to logout type '4'");

            printf("\n\nPlease enter input: ");
            scanf(" %s", userInputString);

            if(strcmp(userInputString, "1") == 0){
              printf("\n##################################################################");
              printf("\nPAGE: LIST ALL BOOKS");

              printf("\n\nWould you like to list all books (y/n): ");
              scanf(" %s", userInputString);

              if(strcmp(userInputString, "y") == 0){
                printf("\n\nList of all books:\n ");

                bookListAll(bookHead);

                printf("\n\n##################################################################");
                printf("\nTYPE 'n' to return to User menu\n");
                scanf(" %s", userInputString);
                printf("\n##################################################################");

              }

              else {
                printf("\nRETURNING!");
              }
            }

            if(strcmp(userInputString, "2") == 0) {
              int bookIndex;
              printf("\n##################################################################");
              printf("\nPAGE: BOOK SEARCH AND BORROW");
              printf("\n\nTo exit type 'exit'");

              while(1){
                printf("\n\nPlease Enter title search query or type 'exit' to exit: ");
                scanf(" %s", userInputString);
                printf("\n\n---------------------------------------------------------------");
                printf("\nSTART OF SEARCH QUERY");
                if(strcmp(userInputString, "exit") == 0) {
                  printf("\n\nRETURNING!");
                  break;
                }

                bookListWithString(bookHead, userInputString);
                printf("\n\nEND OF SEARCH QUERY");
                printf("\n---------------------------------------------------------------");

                printf("\n\nWould you like to borrow a book using the book ID (y/n): ");
                scanf(" %s", userInputString);

                if(strcmp(userInputString, "y") == 0) {
                  while(1){
                    printf("\nEnter the bookID that you would like to borrow or 'exit' to exit': ");
                    scanf(" %s", userInputString);

                    if(strcmp(userInputString, "exit") == 0) {
                      break;
                    }
                    bookIndex = atoi(userInputString);
                    //ensuring index is valid
                    if(bookIndex > 0 && bookIndex <= booksNumOf(bookHead)){
                      printf("\n\n###################################");
                      bookPrintByIndex(&bookHead, (bookIndex - 1));
                      printf("\n\n###################################");

                      printf("\n\n\nIs this the book you would like to borrow(y/n): ");
                      scanf(" %s", userInputString);

                      //user wants to borrow this book
                      if(strcmp(userInputString, "y") == 0) {

                        //making sure user doesn't have more than maximum books
                        if(userLoggedIn->numLoanedBooks >= 4) {
                          printf("\n##################################################################");
                          printf("\nERROR: You already have 4 books, you cannot borrow any more ");
                          printf("\n##################################################################");
                        }


                        //user has less than maximmum books
                        else {
                          for(int i = 0; i < 4; ++i) {
                            if(userLoggedIn->book[i] == NULL) {



                                //fetching book at given index
                                book_t * newBook = bookReturnByIndex(&bookHead, bookIndex - 1);

                                //checking book can be loaned
                                if(newBook->available == true) {//adding book to user books
                                  userLoggedIn->book[i] = newBook;
                                  ++userLoggedIn->numLoanedBooks;

                                  //ensuring that the book cannot be loaned out again
                                  newBook->available = false;
                                  strcpy(newBook->loanedTo, userLoggedIn->emailAddress);
                                  printf("\nYou have successfully borrowed this book!");
                                  break;
                               }

                               //book is not available
                               else {
                                 printf("\n##################################################################");
                                 printf("\nERROR: Book is Not available ");
                                 printf("\n##################################################################");
                                 break;
                               }

                                // userLoggedIn->book[i] = bookToAdd;

                            }
                          }
                       }
                      }
                      break;
                  }
                  else {
                    printf("\n##################################################################");
                    printf("\nERROR: ID is not in range, must be less than total number of books");
                    printf("\n##################################################################");
                  }
                }
               }
              }
             }
             //returning book
             if(strcmp(userInputString, "3") == 0){
               printf("\n##################################################################");
               printf("\nPAGE: RETURN BOOK");
               printf("\n\nTo exit type 'exit'");
               while(1){
                 int bookReturnIndex;

                 printf("\n\nWould you like to return a book (y/n):  ");
                 scanf(" %s", userInputString);

                 if(strcmp(userInputString, "y") == 0) {
                   printf("\n\nList of all borrowed books: ");
                   userPrintAllBooks(userLoggedIn);

                   printf("\n\nPlease enter book number to return or 'exit' to exit: ");
                   scanf(" %s", userInputString);

                   if(strcmp(userInputString, "exit") == 0) {
                     printf("\nRETURING!");
                     break;
                   }

                   else{
                     bookReturnIndex = atoi(userInputString);

                     if(bookReturnIndex > 0 && bookReturnIndex <= userLoggedIn->numLoanedBooks) {
                       int actualIndex;
                       int currentBookIndex = 0;
                       book_t * bookToReturn;

                       for(int i = 0; i < bookReturnIndex; ++i) {
                         if(userLoggedIn->book[i] != NULL){
                           ++currentBookIndex;
                           bookToReturn = userLoggedIn->book[i];
                           actualIndex = i;
                         }
                       }
                       printf("\n###################################");
                       bookPrint(bookToReturn);
                       printf("\n\n###################################");

                       printf("\n\nDo you want to return this book (y/n): ");
                       scanf(" %s", userInputString);

                       if(strcmp(userInputString, "y") == 0) {
                         printf("\nReturning book!");
                         userLoggedIn->book[actualIndex] = NULL;

                         //returning book
                         bookCurrent = bookReturnByTitle(bookHead, bookToReturn->title);
                         --userLoggedIn->numLoanedBooks;

                         bookCurrent->available = true;
                         strcpy(bookCurrent->loanedTo, "NONE");

                       }
                     }

                     else {
                       printf("\n##################################################################");
                       printf("\nERROR: ID is not in range, must be less than total number of books");
                       printf("\n##################################################################");
                     }

                   }
                 }

                 else {
                   printf("\nRETURNING!");
                   break;
                 }
              }
             }




             //logging out
             if(strcmp(userInputString, "4") == 0){
               printf("\n\nRETURNING!");
               printf("\n##################################################################");
               break;
             }
            }
          }
        }
        //password is wrong
        else {
          printf("\n##################################################################");
          printf("\nERROR: Incorrect password, Please enter credentials again");
          printf("\n##################################################################");
        }
      }

      //user has NOT been found
      else{
        printf("\n\nERROR: User not found, please enter again or register");
      }
    }
  }


  if(strcmp(userInputString, "3") == 0) {
      printf("\n\nEXITING PROGRAM!");
      printf("\n##################################################################");
      break;
    }
  }
  bookWriteToFile(bookHead);
  userWriteToFile(userHead);
  return 0;
}
